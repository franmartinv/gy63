#include <stdio.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
//#include "Arduino.h"
#if ARDUINO < 100
	#include <Arduino.h>
#else
	#include <WProgram.h>
#endif
#include "GY63.h"
#include "Wire.h"

#define CSB 5

void setup() {

	GY63 gy63;

	gy63.init(CSB); //Inicializo el sensor

	while (1) {
		printf("Valor de temperatura: %f",gy63.getTemperature(MS561101BA_D2_OSR_4096));
		vTaskDelay(500/portTICK_PERIOD_MS);
		//delay(500);
		printf("Valor de presi�n: %f",gy63.getPressure(MS561101BA_D1_OSR_4096));
	}

	//return 0;
}

void loop () {

}
